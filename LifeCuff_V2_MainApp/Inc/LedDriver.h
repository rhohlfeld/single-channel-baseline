#ifndef _LED_DRIVER_H
#define _LED_DRIVER_H

#include <stdint.h>
#include "main.h"

/*=========================================================================
    I2C ADDRESS/BITS
    -----------------------------------------------------------------------*/
    #define LED_ADDRESS                 (0x02)    
/*=========================================================================*/

/*=========================================================================
    LED Controller Constants 
    -----------------------------------------------------------------------*/
    #define COMSEND  (0x00)
    #define DATASEND  (0x40)     // LCD definitions
    #define LED_DELAY_US (0x01)
/*=========================================================================*/

void LF_LED_Control(int LedStateData[]);
void InitAllLeds(void);










#endif