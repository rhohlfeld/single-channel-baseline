#ifndef _BLE_DRIVER_H
#define _BLE_DRIVER_H

#include "main.h"
#include <stdbool.h>
#include <string.h>

#define BLE_UART_TIMEOUT 1000
#define BLE_STRING_MSG_LENGTH 50

void BLE_RebootModule(void);
void BLE_EnableCmdMode(void);
void BLE_EnableDataMode(void);
void BLE_EnableEchoMode(void);
void BLE_ReadFirmwareVersion(void);
void BLE_ReadDeviceName(void);
void BLE_SetDeviceName(char *DeviceName);
void BLE_ReadDeviceInformation(void);
void BLE_EnableUartTransparentMode(void);
void BLE_TransmitData(char *Data);
void BLE_SetToConnected(void);
void BLE_SetToDisconnected(void);
bool BLE_GetConnectionStatus(void);
void BLE_TurnOff(void);
void BLE_TurnOn(void);
void BLE_Reset(void);

#endif