#ifndef _GPIO_DRIVER_H
#define _GPIO_DRIVER_H

#include <stdbool.h>
#include <stdint.h>
#include "main.h"

void LF_GPIO_EnableBoost(void);
void LF_GPIO_DisableBoost(void);
void LF_GPIO_EnableWifi(void);
void LF_GPIO_DisableWifi(void);
void LF_GPIO_EnableAnalogSupply(void);
void LF_GPIO_DisableAnalogSupply(void);
uint8_t GPIO_ReadModeSwitch(void);
void LF_GPIO_EnableLed(void);
void LF_GPIO_DisableLed(void);
void LF_GPIO_EnableLed(void);
bool LF_GPIO_StartSwitch(void);


#endif