#ifndef _DEBUG_DRIVER_H
#define _DEBUG_DRIVER_H

#include "main.h"
#include <stdbool.h>
#include <string.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>

#define DEBUG_UART_TIMEOUT 100

void serialPutChar(char outputChar);
void serialPutStr(const char *data);
void serialData(uint8_t * buffer,uint8_t size);
void printmsg(char *format,...);

#endif