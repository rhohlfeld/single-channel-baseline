#ifndef _UTILITIES_DRIVER_H
#define _UTILITIES_DRIVER_H
#include <stdint.h>
#include "fatfs.h"
#include "main.h"


void Mount_SD (const TCHAR* path);
void Unmount_SD (const TCHAR* path);
uint32_t DWT_Delay_Init(void);
void DWT_Delay_us(volatile uint32_t microseconds);
void LF_SendToBluetooth(char *msg);
void LF_OpenLifeFile(FIL* fp);
void LF_CloseFile(FIL* fp);
void LF_GetLogFileName(char *filename);
FRESULT LF_OpenAppendFile (FIL* fp,const char* path,const char* data);
FRESULT LF_OpenAppendFileAndBLE (FIL* fp,const char* path,const char* data );
uint32_t LF_GetFileIndex(void);
void LF_SelfTest(bool status);

#endif