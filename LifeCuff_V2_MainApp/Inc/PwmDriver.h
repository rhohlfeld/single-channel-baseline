#ifndef _PWM_DRIVER_H
#define _PWM_DRIVER_H
#include <stdint.h>
#include "main.h"

enum DutyCycle
{
  /*
  TIM_Period = 216000000 / 15000 - 1 = 14400
  
  If you get TIM_Period larger than max timer value (in our case 65535),
  you have to choose larger prescaler and slow down timer tick frequency
  To get proper duty cycle, you have simple equation
  
  pulse_length = ((TIM_Period + 1) * DutyCycle) / 100 - 1
  
  where DutyCycle is in percent, between 0 and 100%
  
  25% duty cycle:     pulse_length = ((14400 + 1) * 25) / 100 - 1 = 3637
  50% duty cycle:     pulse_length = ((14400 + 1) * 50) / 100 - 1 = 7273
  75% duty cycle:     pulse_length = ((14400 + 1) * 75) / 100 - 1 = 10910
  100% duty cycle:    pulse_length = ((14400 + 1) * 100)/ 100 -1  = 14546 
 */
  
  DutyCycle_25  = 3637, 
  DutyCycle_50  = 7273,
  DutyCycle_75  = 10910,
  DutyCycle_100 = 14546,
 
};



void PwmPump__Init(uint8_t dutyCycle);
void PwmValve__Init(uint8_t dutyCycle);
void PumpPwm__Start(void);
void ValvePwm__Start(void);
void PumpPwm__Stop(void);
void ValvePwm__Stop(void);
void PumpPwm_UpdateDutyCycle(uint16_t dutyCycle);
void ValvePwm_UpdateDutyCycle(uint16_t dutyCycle);

#endif