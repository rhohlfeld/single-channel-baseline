#include "DeviceCommands.h"
#include "DebugUart.h"
#include "DeviceCommands.h"
#include "RealTimeClock.h"
#include "main.h"


#define TIMER_DELAY 200
#define TERMINAL_MSG_BUFFER 200

char PodMsg[TERMINAL_MSG_BUFFER];

void commandInvalid(char *Arg)
{
  char *Buffer = "Invalid Command Handler!\n";
  serialPutStr(Buffer); 
}

void commandReadDeviceId(char *data)
{
  /* Read MCU Id, 32-bit access */
  uint8_t i;
  char DeviceId[50];
  uint8_t serialNumber[12]={0};
  uint32_t deviceserial0, deviceserial1, deviceserial2;
  
  deviceserial0 = HAL_GetUIDw0();
  deviceserial1 = HAL_GetUIDw1();
  deviceserial2 = HAL_GetUIDw2();
  memset(serialNumber, 0, 12);
  memcpy(&serialNumber[0], &deviceserial0, 4);
  memcpy(&serialNumber[4], &deviceserial1, 4);
  memcpy(&serialNumber[8], &deviceserial2, 4);
  serialPutStr("OK,DEVICEID,");
  for(i=0; i<12; i++)
  {
    i < 11 ? sprintf((char*)DeviceId,"%x", serialNumber[i]) : sprintf((char*)DeviceId,"%d\n", serialNumber[i]);
    serialPutStr(DeviceId);
  }
}

void commandGetTime(char *data)
{
  serialPutStr("OK,GETTTIME,");
  Rtc_GetTime();
}


uint8_t hour;
uint8_t minutes;
uint8_t second; 
uint8_t dayoftheweek;
uint8_t month; 
uint8_t day; 	
uint8_t year; 

void commandSetTime(char *data)
{
  uint8_t Values[10];
  const char s[2] = ",";
  
  char *token;
  
  uint8_t count =0;
  
  /* get the first token */
  token = strtok(data, s);
  
  /* walk through other tokens */
  while( token != NULL ) 
  {
    Values[count] = atoi(token);
    token = strtok(NULL, s);
    count++;
  }
  
  hour = Values[0];
  minutes = Values[1];
  second =  Values[2];
  dayoftheweek = Values[3];
  month = Values[4];
  day =  Values[5];		
  year =  Values[6];
  
 
  Rtc_SetTime(hour,minutes,second,dayoftheweek,month,day,year);
  
  sprintf(PodMsg,"WeekDay:%d,Month:%d,Date:%d,Year:%d",dayoftheweek,month,day,year);
  serialPutStr(PodMsg);
}


void CommandInvalidCmd(char *Arg)
{
  char *Buffer = "BLE Invalid Command Handler!\n";
  serialPutStr(Buffer); 
}





