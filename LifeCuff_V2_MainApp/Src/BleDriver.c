#include "BleDriver.h"

extern UART_HandleTypeDef huart1;
bool connectStatus = false;
char BLEStringBuffer[BLE_STRING_MSG_LENGTH];

#define BLE_UART huart1

/**
* @brief Function to BLE_EnableCmdMode.
* @param argument: None
* @retval None
*/
void BLE_EnableCmdMode(void)
{
  const char *cmdString = "$$$";
  HAL_UART_Transmit(&BLE_UART,(uint8_t*)cmdString, strlen(cmdString),BLE_UART_TIMEOUT);
}

/**
* @brief Function to BLE_EnableDataMode.
* @param argument: None
* @retval None
*/
void BLE_EnableDataMode(void)
{
  const char *cmdString = "---\r";
  HAL_UART_Transmit(&BLE_UART,(uint8_t*)cmdString, strlen(cmdString),BLE_UART_TIMEOUT);
}

/**
* @brief Function to BLE_EnableEchoMode.
* @param argument: None
* @retval None
*/
void BLE_EnableEchoMode(void)
{
  const char *cmdString = "+\r";
  HAL_UART_Transmit(&BLE_UART,(uint8_t*)cmdString, strlen(cmdString),BLE_UART_TIMEOUT);
}

/**
* @brief Function to BLE_ReadFirmwareVersion.
* @param argument: None
* @retval None
*/
void BLE_ReadFirmwareVersion(void)
{
  const char *cmdString = "V\r";
  HAL_UART_Transmit(&BLE_UART,(uint8_t*)cmdString, strlen(cmdString),BLE_UART_TIMEOUT);
}

/**
* @brief Function to BLE_ReadDeviceName.
* @param argument: None
* @retval None
*/
void BLE_ReadDeviceName(void)
{
  const char *cmdString = "GN\r";
  HAL_UART_Transmit(&BLE_UART,(uint8_t*)cmdString, strlen(cmdString),BLE_UART_TIMEOUT);
}

/**
* @brief Function to BLE_SetDeviceName.
* @param argument: DeviceName
* @retval None
*/
void BLE_SetDeviceName(char *DeviceName)
{
  strcpy(BLEStringBuffer,"SN,");
  strcat(BLEStringBuffer,DeviceName);
  strcat(BLEStringBuffer,"\r");
  HAL_UART_Transmit(&BLE_UART,(uint8_t*)BLEStringBuffer, strlen(BLEStringBuffer)
                    ,BLE_UART_TIMEOUT);
  
}

/**
* @brief Function to BLE_ReadDeviceInformation.
* @param argument: DeviceName
* @retval None
*/
void BLE_ReadDeviceInformation()
{
  const char *cmdString = "D\r";
  HAL_UART_Transmit(&BLE_UART,(uint8_t*)cmdString, strlen(cmdString),BLE_UART_TIMEOUT);
  
}

/**
* @brief Function to BLE_EnableUartTransparentMode.
* @param argument: DeviceName
* @retval None
*/
void BLE_RebootModule(void)
{
  const char *cmdString = "R,1\r";
  HAL_UART_Transmit(&BLE_UART,(uint8_t*)cmdString, strlen(cmdString),BLE_UART_TIMEOUT);
  
}

/**
* @brief Function to BLE_EnableUartTransparentMode.
* @param argument: DeviceName
* @retval None
*/
void BLE_EnableUartTransparentMode(void)
{
  const char *cmdString = "SS,C0\r";
  HAL_UART_Transmit(&BLE_UART,(uint8_t*)cmdString, strlen(cmdString),BLE_UART_TIMEOUT);
  
  
}

void BLE_TransmitData(char *Data)
{
  HAL_UART_Transmit(&BLE_UART,(uint8_t*)Data, strlen(Data),BLE_UART_TIMEOUT);
}

void BLE_TurnOff(void)
{
	
}

void BLE_TurnOn(void)
{
	
}


void BLE_Reset(void)
{
  BLE_TurnOff();
  HAL_Delay(250);
  BLE_TurnOn();
  
}

bool BLE_GetConnectionStatus(void)
{
  return connectStatus;
}

void BLE_SetToDisconnected(void)
{
  connectStatus = false;
}

void BLE_SetToConnected(void)
{
  connectStatus = true;
}
