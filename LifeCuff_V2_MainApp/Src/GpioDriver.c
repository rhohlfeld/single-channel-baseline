#include "GpioDriver.h"
#include "DebugUart.h"


uint8_t SwitchMode[3]={0};
uint8_t SwitchModeResult = 0;

static uint8_t GPIO_SwitchOneOn(void);
static uint8_t GPIO_SwitchTwoOn(void);
static uint8_t GPIO_SwitchThreeOn(void);

/**
* @brief  Enable BackLight
*/



/**
* @brief  Enable Wifi
*/

void LF_GPIO_EnableWifi(void)
{
  HAL_GPIO_WritePin(WIFI_CE_GPIO_Port, WIFI_CE_Pin, GPIO_PIN_SET);
}

/**
* @brief  Disable Wifi
*/

void LF_GPIO_DisableWifi(void)
{
  HAL_GPIO_WritePin(WIFI_CE_GPIO_Port, WIFI_CE_Pin, GPIO_PIN_RESET);
}

/**
* @brief  Enable Boost
*/

void LF_GPIO_EnableBoost(void)
{
  HAL_GPIO_WritePin(Main_PWR_GPIO_Port, Main_PWR_Pin, GPIO_PIN_SET);
}

/**
* @brief  Disable Boost
*/
void LF_GPIO_DisableBoost(void)
{
  HAL_GPIO_WritePin(Main_PWR_GPIO_Port, Main_PWR_Pin, GPIO_PIN_RESET);
  
}

/**
* @brief  Enable Analog Supply
*/
void LF_GPIO_EnableAnalogSupply(void)
{
  HAL_GPIO_WritePin(VCC_A2_EN_PWR_GPIO_Port, VCC_A2_EN_PWR_Pin, 
                    GPIO_PIN_SET);
}


/**
* @brief  Disable Analog Supply
*/
void LF_GPIO_DisableAnalogSupply(void)
{
  HAL_GPIO_WritePin(VCC_A2_EN_PWR_GPIO_Port, VCC_A2_EN_PWR_Pin, 
                    GPIO_PIN_RESET);
}




/**
* @brief  Disable Led
*/
void LF_GPIO_DisableLed(void)
{
  HAL_GPIO_WritePin(LED_OE_GPIO_Port, LED_OE_Pin, 
                    GPIO_PIN_RESET);
}





/**
* @brief  Read Switch One State
*/
static uint8_t GPIO_SwitchOneOn(void)
{
  uint8_t value = 0;
  if(HAL_GPIO_ReadPin(D_SW1_Dip_Switch_GPIO_Port,D_SW1_Dip_Switch_Pin)
     ==GPIO_PIN_SET)
  {
    value = 1;
  }
  return value; 
  
}
/**
* @brief  Read Switch Two State
*/
static uint8_t GPIO_SwitchTwoOn(void)
{
  uint8_t value = 0;
  if(HAL_GPIO_ReadPin(D_SW2_Dip_Switch_GPIO_Port,D_SW2_Dip_Switch_Pin)
     ==GPIO_PIN_SET)
  {
    value = 1;
  }
  return value;
  
}
/**
* @brief  Read Switch Three State
*/
static uint8_t GPIO_SwitchThreeOn(void)
{
  uint8_t value = 0;
  if(HAL_GPIO_ReadPin(D_SW3_Dip_Switch_GPIO_Port,D_SW3_Dip_Switch_Pin)
     ==GPIO_PIN_SET)
  {
    value = 1;
  }
  return value;
  
}


uint8_t GPIO_ReadModeSwitch(void)
{
  SwitchMode[0]=GPIO_SwitchOneOn() ; 
  SwitchMode[1]=GPIO_SwitchTwoOn() ; 
  SwitchMode[2]=GPIO_SwitchThreeOn() ; 
  SwitchModeResult = (SwitchMode[0]) + (2*SwitchMode[1]) + (4*SwitchMode[2]);
  return SwitchModeResult; 
}


void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  
  switch(GPIO_Pin)
  {
    /*Activation Switch Pressed Handler*/
  case GPIO_PIN_1: 
   
   
    break;
    
  default:
    serialPutStr("Invalid ISR Event\n");
    break; 
  }
}


void LF_GPIO_EnableLed(void)
{
  HAL_GPIO_WritePin(LED__RST_GPIO_Port, LED__RST_Pin, 
                    GPIO_PIN_SET);
  
  HAL_GPIO_WritePin(LED_OE_GPIO_Port, LED_OE_Pin, 
                    GPIO_PIN_RESET);
}

 /**
* @brief  Read Start Switch
*/

bool LF_GPIO_StartSwitch(void)
{
  bool status = true;
   if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_1)==GPIO_PIN_SET)
   {
     status = false;
   }
   return status;
 
}



