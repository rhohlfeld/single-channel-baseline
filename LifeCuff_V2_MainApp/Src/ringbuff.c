

#include <stdint.h>    
#include <stdbool.h>   
#include "ringbuff.h"

// Suppress messages about unordered accesses to volatile objects
#pragma diag_remark=Pa082

#define INIT_COOKIE   (0x0BADC0DE)
#define INIT_OK(me)   (INIT_COOKIE == me->cookie)

// *****************************************************************
void RingBuffer_init(RingBuffer *me)
{

  me->cookie    = INIT_COOKIE;
  me->count     = 0UL;
  me->ritedex   = 0UL;
  me->readdex   = 0UL;
  me->capacity  = (sizeof(me->buffer) / (sizeof(me->buffer[0])));
}

// *****************************************************************
bool RingBuffer_empty(RingBuffer *me)
{
 
  return (0 == me->count);
}

// *****************************************************************
bool RingBuffer_full(RingBuffer *me)
{
  
  return (me->count == me->capacity);
}

// *****************************************************************
void RingBuffer_put(RingBuffer *me, char val)
{
 
  // If full, silently discard data
  if (!RingBuffer_full(me))
  {
    me->count++;
    me->buffer[me->ritedex++] = val;
   
    if (me->ritedex == me->capacity)  // index wrap
    {
      me->ritedex = 0;
    }
  }
}

// *****************************************************************
char RingBuffer_get(RingBuffer *me)
{
  
  if (RingBuffer_empty(me))
  {
    return (char)0;
  }

  me->count--;
  char val = me->buffer[me->readdex++];
  if (me->readdex == me->capacity)
  {
    me->readdex = 0;
  }
  return val;
}

// *****************************************************************
uint32_t RingBuffer_capacity(RingBuffer *me)
{
 
  return me->capacity;
}

// *****************************************************************
uint32_t RingBuffer_space(RingBuffer *me)
{
 
  return (me->capacity - me->count);
}
