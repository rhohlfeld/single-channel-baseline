#include "PwmDriver.h"

extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;

uint16_t CalculatePulseWidth(uint8_t DutyCycle,uint16_t Period)
{
  return (((Period + 1) * DutyCycle) / (99));
}


/**
* @brief  Pwm PumpInit.
*/
void PwmValve__Init(uint8_t dutyCycle)
{
   /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 0;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 10799;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = CalculatePulseWidth(dutyCycle,10799);
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */
  HAL_TIM_MspPostInit(&htim2);

}

/**
* @brief  Pwm Valve Init.
*/
void PwmPump__Init(uint8_t dutyCycle)
{
  /* USER CODE BEGIN TIM3_Init 0 */
  
  /* USER CODE END TIM3_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM3_Init 1 */
  
  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 0;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 10799;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = CalculatePulseWidth(dutyCycle,10799);
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */
  
  /* USER CODE END TIM3_Init 2 */
  HAL_TIM_MspPostInit(&htim3);
  
}

/**
* @brief  Start Valve.
*/
void ValvePwm__Start(void)
{
  HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_2);
}

/**
* @brief  Start Pump.
*/
void PumpPwm__Start(void)
{
  HAL_TIM_PWM_Start(&htim3,TIM_CHANNEL_2);
}

/**
* @brief  Stop Valve.
*/
void ValvePwm__Stop(void)
{
  HAL_TIM_PWM_Stop(&htim2,TIM_CHANNEL_2);
}

/**
* @brief  Stop Pump.
*/
void PumpPwm__Stop(void)
{
  HAL_TIM_PWM_Stop(&htim3,TIM_CHANNEL_2);
}

/**
* @brief  Modify Valve Duty Cycle.
*/
void ValvePwm_UpdateDutyCycle(uint16_t dutyCycle)
{
  __HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_2, dutyCycle);
}

/**
* @brief  Modify Pump Duty Cycle.
*/
void PumpPwm_UpdateDutyCycle(uint16_t dutyCycle)
{
  __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, dutyCycle);
}
