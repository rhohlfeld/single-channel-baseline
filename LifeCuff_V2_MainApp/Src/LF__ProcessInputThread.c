#include "main.h"
#include "stm32f7xx_hal.h"
#include "PwmDriver.h"
#include "LcdDriver.h"
#include "DeviceCommands.h"
#include "DriverUtilities.h"
#include "DebugUart.h"
#include "BleDriver.h"
#include "GpioDriver.h"
#include "ringbuff.h"
#include <string.h>


#define RX_WORD_LENGTH 100 
#define MAX_COMMAND_LEN 100
#define DEVICE_COMMAND_TABLE_LEN 35
#define BLE_COMMAND_TABLE_LEN 35
#define COMMAND_TABLE_SIZE (30)
#define UART_DMA_RECEIVE_SIZE 1
#define TO_UPPER(x) (((x >= 'a') && (x <= 'z')) ? ((x) - ('a' - 'A')) : (x))
#define TERMINAL_MSG_BUFFER 400

typedef struct
{
  char *name;
  void (*function)(char *Arg);
}command_t;


command_t const bleCommandTable[BLE_COMMAND_TABLE_LEN] =
{
  {"DEVICEID",commandReadDeviceId},
  {"GETTIME",commandGetTime},
  {"SETTIME",commandSetTime},
  {NULL, CommandInvalidCmd }
};

command_t const CommandTable[BLE_COMMAND_TABLE_LEN] =
{
  {"DEVICEID",commandReadDeviceId},
  {"GETTIME",commandGetTime},
  {"SETTIME",commandSetTime},
  {NULL, CommandInvalidCmd }
};



extern UART_HandleTypeDef huart2;
extern TX_QUEUE Event_Queue;
extern TX_QUEUE BLE_Queue;
extern TX_QUEUE Terminal_Queue;
extern ADC_HandleTypeDef hadc1;
//extern TX_SEMAPHORE            semaphore_0;

extern bool QuitDither;
extern FIL fp; 
extern char LogFileName[20]; 
volatile uint8_t convCompleted = 0;
uint32_t sensorValues[3];
char bleData[1];
char uartData[1];
static RingBuffer BleRxBuffer;
static RingBuffer uartRxBuffer; 
static char bleCommandBuffer[MAX_COMMAND_LEN + 1];
static char CommandBuffer[MAX_COMMAND_LEN + 1];


void BleInvalidCmd(char *Arg);
void BleStartTherapy(char *Arg);

int BleLineBuildCommand(char nextChar);
void BleLineProcessCommand(char * buffer);


int CmdLineBuildCommand(char nextChar);
void CmdLineProcessCommand(char * buffer);

//---------------------------------------------------------------
// Private Variables
//---------------------------------------------------------------
static uint8_t StartSwitch_state = 0;

//---------------------------------------------------------------
// Private Functions
//---------------------------------------------------------------

static void Process_StartAbortSwitchState(void)
{
  
  static bool startState = false; 
  if(!startState)
  {
    StartDataAcquireTimer();
    LF__SendMailToEventThread(START_MSG);
    startState = true;
  }
  else
  {
    //Abort 
    StopDataAcquireTimer();
    tx_thread_sleep(3000);
    if(LF_GPIO_StartSwitch())
    {
      
      QuitDither = true; 
      PumpPwm__Stop();
      ValvePwm__Stop();
      LF_OpenAppendFileAndBLE(&fp,LogFileName,"Aborted by User"); 
      f_close(&fp);
      LF__ST7036__LCDclear();
      LF__ST7036__LCDwrite(" Aborted by User ",0);
      tx_thread_sleep(5000);
      NVIC_SystemReset(); 
      startState = false;
      
    }
    
  }
}

static void Remote_StartStop(void)
{
  static bool startState = false; 
  if (!startState)
  {      
    LF__SendMailToEventThread(START_MSG);
    startState = true;
  }
  else
  {
    QuitDither = true; 
    PumpPwm__Stop();
    ValvePwm__Stop();
    LF_OpenAppendFileAndBLE(&fp,LogFileName,"Aborted by User"); 
    f_close(&fp);
    LF__ST7036__LCDclear();
    LF__ST7036__LCDwrite(" Aborted by User ",0);
    tx_thread_sleep(5000);
    NVIC_SystemReset(); 
    startState = false;
  }
  
  
}

static void SwitchHandler__Process(void)
{
  uint8_t debounce_count = 0;
  uint8_t press_count    = 0;
  
  StartSwitch_state = LF_GPIO_StartSwitch();
  if (StartSwitch_state)
  {
    while (StartSwitch_state)
    {
      if (debounce_count == 0)
      {
        press_count++;
        Process_StartAbortSwitchState();	
        debounce_count++;
      }      
      StartSwitch_state = LF_GPIO_StartSwitch();
    }
    debounce_count = 0;
  }
}

void Terminal_entry(ULONG thread_input)
{
  char uartData[1];
  int CommandReady = 0;
  LF_SendToBluetooth("Terminal Thread Started\n");
  HAL_UART_Receive_IT(Get_DebugHandle(),(uint8_t*)uartData,UART_DMA_RECEIVE_SIZE);
 
  while(1)
  {  
    if(!GetTherapyStatus())
    {
        tx_queue_receive(&Terminal_Queue,uartData,TX_WAIT_FOREVER);
        CommandReady = CmdLineBuildCommand(uartData[0]);
        if(CommandReady)
        {
          CmdLineProcessCommand(CommandBuffer);
          CommandReady = 0;
        }
    }
    tx_thread_sleep(1);
  }
  
}
void NetworkManager_entry(ULONG thread_input)
{
  char bleData[1];
  int bCommandReady = 0;
  HAL_UART_Receive_IT(Get_BleHandle(),(uint8_t*)bleData,UART_DMA_RECEIVE_SIZE);
  
  while(1)
  {
    if(!GetTherapyStatus())
    {
        tx_queue_receive(&BLE_Queue,bleData,TX_WAIT_FOREVER);
        bCommandReady = BleLineBuildCommand(bleData[0]);
        if(bCommandReady)
        {
          BleLineProcessCommand(bleCommandBuffer);
          bCommandReady = 0;
        }
    }
    tx_thread_sleep(1);
  }
}


void    ProcessInputThread_entry(ULONG thread_input)
{ 
  while(1)
  {
    SwitchHandler__Process();
    tx_thread_sleep(1);
  }
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
  convCompleted = 1;
}



int BleLineBuildCommand(char nextChar)
{
  static uint8_t idx = 0;
  /* Don't store any new line characters or spaces. */
  if ((nextChar == '\n') || (nextChar == ' ') || (nextChar == '\t'))
  {
    return false;
  }
  /* The completed command has been received. Replace the final carriage
  * return character with a NULL character to help with processing the
  * command. */
  if (nextChar == '\r')
  {
    bleCommandBuffer[idx] = '\0';
    idx = 0;
    return true;
  }
  /* Convert the incoming character to uppercase. This matches the case
  * of commands in the command table. Then store the received character
  * in the command buffer. */
  bleCommandBuffer[idx] = TO_UPPER(nextChar);
  idx++;
  /* If the command is too long, reset the index and process
  * the current command buffer. */
  if (idx > MAX_COMMAND_LEN)
  {
    idx = 0;
    return true;
  }
  
  return false;
  
  
}
void BleLineProcessCommand(char * buffer)
{
  int bCommandFound = false;
  int idx;
  
  char *Com= strtok(buffer,"+");
  char *Arg = strtok(NULL,"+");
  
  /* Search for the command in the command table until it is found or
  * the end of the table is reached. If the command is found, break
  * out of the loop. */
  for (idx = 0; bleCommandTable[idx].name != NULL; idx++)
  {
    if (strncmp(bleCommandTable[idx].name, buffer,strlen(bleCommandTable[idx].name)) == 0)
    {
      bCommandFound = true;
      (*bleCommandTable[idx].function)(Arg);
      break;
    }
  }
  /* If the command was found, call the command function. Otherwise,
  * output an error message. */
  if (!bCommandFound)
  {
    LF_SendToBluetooth("\r\nBLE Command not found.\r\n");
    BLE_TransmitData("\r\nBLE Command not found.\r\n");
    
  }
  
}

int CmdLineBuildCommand(char nextChar)
{
  static uint8_t idx = 0;
  /* Don't store any new line characters or spaces. */
  if ((nextChar == '\n') || (nextChar == ' ') || (nextChar == '\t'))
  {
    return false;
  }
  /* The completed command has been received. Replace the final carriage
  * return character with a NULL character to help with processing the
  * command. */
  if (nextChar == '\r' || nextChar == '#')
  {
    CommandBuffer[idx] = '\0';
    idx = 0;
    return true;
  }
  /* Convert the incoming character to uppercase. This matches the case
  * of commands in the command table. Then store the received character
  * in the command buffer. */
  CommandBuffer[idx] = TO_UPPER(nextChar);
  idx++;
  /* If the command is too long, reset the index and process
  * the current command buffer. */
  if (idx > MAX_COMMAND_LEN)
  {
    idx = 0;
    return true;
  }
  
  return false;
  
  
}
void CmdLineProcessCommand(char * buffer)
{
  int bCommandFound = false;
  int idx;
  
  char *Com= strtok(buffer,"+");
  char *Arg = strtok(NULL,"+");
  
  /* Search for the command in the command table until it is found or
  * the end of the table is reached. If the command is found, break
  * out of the loop. */
  for (idx = 0; CommandTable[idx].name != NULL; idx++)
  {
    if (strncmp(CommandTable[idx].name, buffer,strlen(CommandTable[idx].name)) == 0)
    {
      bCommandFound = true;
      (*CommandTable[idx].function)(Arg);
      break;
    }
  }
  /* If the command was found, call the command function. Otherwise,
  * output an error message. */
  if (!bCommandFound)
  {
    serialPutStr("\r\Command not found.\r\n");
  }
  
}


void BleStartTherapy(char *Arg)
{
  Remote_StartStop(); 
}


void BleInvalidCmd(char *Arg)
{
  char *Buffer = "BLE Invalid Command Handler!\n";
  LF_SendToBluetooth(Buffer); 
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  if(huart->Instance==USART2)
  {
    HAL_UART_Receive_IT(Get_DebugHandle(),(uint8_t*)uartData,UART_DMA_RECEIVE_SIZE);	
    tx_queue_send(&Terminal_Queue,uartData, TX_NO_WAIT); 
  }
  if(huart->Instance==USART1)
  {
    HAL_UART_Receive_IT(Get_BleHandle(),(uint8_t*)bleData,UART_DMA_RECEIVE_SIZE);	
    tx_queue_send(&BLE_Queue,bleData, TX_NO_WAIT); 
  }
  
}




