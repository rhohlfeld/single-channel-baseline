#include "DebugUart.h"
#include "main.h"



void serialPutStr(const char *data)
{
  HAL_UART_Transmit(Get_DebugHandle(),(uint8_t *)data, strlen(data),DEBUG_UART_TIMEOUT);
}

void serialPutChar(char outputChar)
{
  char Data[1] ;
  Data[0]	= outputChar;
  HAL_UART_Transmit(Get_DebugHandle(),(uint8_t*)Data, 1,DEBUG_UART_TIMEOUT);
  
}


void serialData(uint8_t * buffer,uint8_t size)
{
  HAL_UART_Transmit(Get_DebugHandle(),buffer, size,DEBUG_UART_TIMEOUT);
}

void printmsg(char *format,...)
{
  
  char str[80];
  
  /*Extract the the argument list using VA apis */
  va_list args;
  va_start(args, format);
  vsprintf(str, format,args);
  HAL_UART_Transmit(Get_DebugHandle(),(uint8_t *)str, strlen(str),HAL_MAX_DELAY);
  va_end(args);
  
}

